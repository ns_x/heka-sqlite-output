package plugins

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"github.com/mozilla-services/heka/message"
	. "github.com/mozilla-services/heka/pipeline"
	"strings"
)

type SQLiteOutputConfig struct {
	// Data Source Name string with URI filename
	// i.e.:
	// test.db
	// file:test.db?cache=shared&mode=memory
	DSN string `toml:"dsn"`

	// Name of the SQLite table in which the messages will be stored
	Table string `toml:"table"`

	// The columns that need to be stored in the database
	Columns []string `toml:"columns"`

	// the SQLite journal_mode
	JournalMode string `toml:"journal_mode"`
}

type SQLiteOutput struct {
	DSN     string
	Table   string
	Columns []string
	db      *sql.DB
	stmt    *sql.Stmt
}

func (self *SQLiteOutput) ConfigStruct() interface{} {
	return &SQLiteOutputConfig{JournalMode: "DELETE"}
}

func (self *SQLiteOutput) ColumnType(columnName string) string {
	if columnName == "timestamp" || columnName == "severity" || columnName == "pid" {
		return "integer"
	} else {
		return "text"
	}
}

func (self *SQLiteOutput) SetJournalMode(journalMode string) error {
	pragma := fmt.Sprintf("PRAGMA journal_mode = %s;", self.Table, journalMode)
	_, err := self.db.Exec(pragma)
	return err
}

func (self *SQLiteOutput) CreateTable() error {
	columns := make([]string, len(self.Columns))
	for i := range self.Columns {
		columnName := self.Columns[i]
		columns[i] = fmt.Sprintf("%s %s", columnName, self.ColumnType(columnName))
	}
	createStatement := fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (%s);", self.Table, strings.Join(columns, ", "))
	_, err := self.db.Exec(createStatement)
	return err
}

func (self *SQLiteOutput) AddColumns() error {
	inDatabase := make(map[string]bool, len(self.Columns))
	tableInfo := fmt.Sprintf("PRAGMA table_info(%s);", self.Table)
	rows, err := self.db.Query(tableInfo)
	if err != nil {
		return err
	}
	for rows.Next() {
		var id int
		var columnName string
		var dataType string
		var canBeNull bool
		var defaultValue sql.NullString
		var pk int
		err = rows.Scan(&id, &columnName, &dataType, &canBeNull, &defaultValue, &pk)
		if err != nil {
			return err
		}
		inDatabase[columnName] = true
	}

	for i := range self.Columns {
		columnName := self.Columns[i]
		if !inDatabase[columnName] {
			alterStatement := fmt.Sprintf("ALTER TABLE %s ADD COLUMN %s %s;", self.Table, columnName, self.ColumnType(columnName))
			_, err := self.db.Exec(alterStatement)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (self *SQLiteOutput) Init(config interface{}) (err error) {
	conf := config.(*SQLiteOutputConfig)
	if conf.DSN == "" {
		return fmt.Errorf("DSN required")
	}
	self.DSN = conf.DSN

	if conf.Table == "" {
		return fmt.Errorf("Table required")
	}
	self.Table = conf.Table

	if conf.Columns == nil {
		return fmt.Errorf("Columns required")
	}
	self.Columns = conf.Columns

	self.db, err = sql.Open("sqlite3", self.DSN)
	if err != nil {
		return err
	}

	// create the table if it does not yet exist
	err = self.CreateTable()
	if err != nil {
		return err
	}

	// set the journal_mode
	err = self.SetJournalMode(conf.JournalMode)
	if err != nil {
		return err
	}

	// check if all the specified columns exist
	// and if not, add them
	err = self.AddColumns()
	if err != nil {
		return err
	}

	values := make([]string, 0, len(self.Columns))
	for _ = range self.Columns {
		values = append(values, "?")
	}

	insertStatement := fmt.Sprintf("INSERT INTO %s(%s) values (%s);", self.Table, strings.Join(self.Columns, ", "), strings.Join(values, ", "))
	self.stmt, err = self.db.Prepare(insertStatement)
	if err != nil {
		return err
	}
	return nil
}

func (self *SQLiteOutput) Run(or OutputRunner, h PluginHelper) (err error) {
	inChan := or.InChan()

	var (
		pack *PipelinePack
		msg  *message.Message
	)
	for pack = range inChan {
		msg = pack.Message
		values := make([]interface{}, 0, len(self.Columns))
		for _, v := range self.Columns {
			var value interface{}
			switch v {
			case "uuid":
				value = msg.GetUuid()
			case "timestamp":
				value = msg.GetTimestamp()
			case "type":
				value = msg.GetType()
			case "logger":
				value = msg.GetLogger()
			case "severity":
				value = msg.GetSeverity()
			case "payload":
				value = msg.GetPayload()
			case "pid":
				value = msg.GetPid()
			case "hostname":
				value = msg.GetHostname()
			default:
				value, _ = msg.GetFieldValue(v)
			}
			values = append(values, value)
		}
		_, err := self.stmt.Exec(values...)
		if err != nil {
			return err
		}
		pack.Recycle()
	}

	self.db.Close()
	return nil
}

func init() {
	RegisterPlugin("SQLiteOutput", func() interface{} {
		return new(SQLiteOutput)
	})
}
